%{
    #include <stdio.h> /* printf, fprintf */
    #include <math.h> /* pow, atan, sin, cos, exp, log, sqrt */
    #include <string.h> /* strcmp */
    #include "calc.h" /* symrec, table_sym */
    int yylex(void);
    void yyerror(char const *);
    double calculer(double a, double b, chaine op);
%}

/**
 * Ce bloc permet d'inclure les définitions des types contenus dans calc.h
 * dans projet.tab.h. Ainsi, ils peuvent être utilisés dans l'analyseur lexical.
 */
%code requires {
    #include "calc.h"
}

/**
 * Ce bloc permet la déclaration de YYSTYPE, les types pouvant êtres pris par yylval
 * dans l'analyseur lexical. Il s'agit d'une union avec les différents types.
 * Chaque groupe lexical se voit affecter un type.
 */
%define api.value.type union
%token <chaine>  OPERATEUR LIAISON
%token <symrec*> VARIABLE FONCTION
%token <double>  NOMBRE
%type <double> expression

/**
 * Ce bloc détermine l'ordre de priorité des opérateurs.
 * Cela sert aussi à leurs déclaration en tant que lexèmes.
 */ 
%precedence '='
%left '+' '-'
%left '*' '/'
%precedence NEG
%right '^'

/**
 * Il y a exactement 5 conflits shift/reduce dans notre grammaire.
 * Ces conflits sont entre : 
 *  - expression 'op' expression
 *  - OPERATEUR expression LIAISON expression
 * Cette ligne sert à ignorer l'avertissement de yacc.
 */
%expect 5

%%

programme:
    %empty /* On ignore un programme vide */
    | programme entree '\n'
    ;

entree:
    %empty /* On ignore une ligne vide */
    | expression  { printf("%g\n", $1); }
    | error { yyerrok; }
    ;

expression:
    NOMBRE                                      { $$ = $1; }
    | VARIABLE                                  { $$ = $1->valeur.var; }
    | VARIABLE '=' expression                   { $$ = $3; $1->valeur.var = $3; }
    | FONCTION '(' expression ')'               { $$ = (*($1->valeur.fnctptr))($3); }
    | expression '+' expression                 { $$ = $1 + $3; } 
    | expression '-' expression                 { $$ = $1 - $3; } 
    | expression '*' expression                 { $$ = $1 * $3; } 
    | expression '/' expression                 { $$ = $1 / $3; }
    | '-' expression %prec NEG                  { $$ = -$2; }
    | expression '^' expression                 { $$ = pow($1, $3);}
    | '(' expression ')'                        { $$ = $2; }
    | OPERATEUR expression LIAISON expression   { $$ = calculer($2, $4, $1); }
    ;
    

%%

/**
 * Cette partie déinit une fonction d'initialisation de la table de symboles.
 * La table est initialisée avec des types FONCTION, le reste sera rajouté comme VARIABLE.
 */
struct init {
    char const *fnom;
    double (*fonction)(double);
};

struct init const fncts_arith[] = {
    {"atan", atan},
    {"cos", cos},
    {"sin", sin},
    {"exp", exp},
    {"ln", log},
    {"sqrt", sqrt},
    {0, 0}
};

/* Table des symboles (voir calc.h) */
symrec *table_sym;

void init_table() {
    int i;
    for (i = 0; fncts_arith[i].fnom != 0; i++) {
        symrec *ptr = putsym(fncts_arith[i].fnom, FONCTION);
        ptr->valeur.fnctptr = fncts_arith[i].fonction;
    } 
}

/**
 * Affichage des erreurs.
 */
void yyerror(char const *s) {
    fprintf(stderr, "%s\n", s);
}

/**
 * Calcul avec les opérateurs en langage naturel.
 */
double calculer(double a, double b, chaine op) {
    if (strcmp(op, "additionner") == 0) {
        return a + b;
    } else if (strcmp(op, "soustraire") == 0 ) {
        return a - b;
    } else if (strcmp(op, "multiplier") == 0) {
        return a * b;
    } else {
        if (fabs(b) > 1.e-7) {
            return a / b;
        } else {
            return INFINITY;
        }
    }
}

int main(int argc, char *argv[]) {
    init_table(); /* On initialise la table avec les fonctions */
    return yyparse(); /* On lance l'analyseur syntaxique */
}
