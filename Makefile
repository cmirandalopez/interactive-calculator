LEX=flex
YY=bison
CC=gcc

exe: projet.tab.c projet.lex.o calc.o
	$(CC) -o projet projet.tab.c projet.lex.o calc.o -ly -ll -lm

calc.o: calc.c
	$(CC) calc.c -c

projet.lex.o:
	$(LEX) -o projet.lex.c -l projet.l
	$(CC) projet.lex.c -c

projet.tab.c:
	$(YY) -d projet.y

clean:
	rm projet
	rm calc.o
	rm projet.lex.c projet.lex.o
	rm projet.tab.c projet.tab.h
