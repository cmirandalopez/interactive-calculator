#include <stdlib.h>
#include <string.h>
#include "calc.h"

symrec *putsym(char const *nom_sym, int type_sym) {
    symrec *ptr = (symrec *) malloc(sizeof(symrec));
    ptr->nom = (char *) malloc(strlen(nom_sym) + 1);
    strcpy(ptr->nom, nom_sym);
    ptr->type = type_sym;
    ptr->valeur.var = 0;
    ptr->suivant = (struct symrec *) table_sym;
    table_sym = ptr;
    return ptr;
}

symrec *getsym(char const *nom_sym) {
    symrec *ptr;
    for (ptr = table_sym; ptr != (symrec *) 0; ptr = (symrec *) ptr->suivant) {
        if (strcmp(ptr->nom, nom_sym) == 0) {
            return ptr;
        }
    }
    return 0;
}