#ifndef CALC_H
#define CALC_H

#include <math.h>

/* Type fonction */
typedef double (*fonction_t)(double);

/* Types de données pour les éléments dans la table de symboles */
struct symrec {
    char *nom; // Nom du symbole
    int type;  // Type du symbole : VARIABLE, FONCTION
    union {
        double var; // Valeur si VARIABLE
        fonction_t fnctptr; // Valeur si FONCTION
    } valeur;
    struct symrec *suivant;
};

typedef struct symrec symrec;
typedef char chaine[20];

/* Table de symboles : liste chaînée de symrec */
extern symrec *table_sym;

symrec *putsym (char const *, int);
symrec *getsym (char const *);

#endif