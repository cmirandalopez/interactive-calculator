%{
	#include <stdio.h> /* sscanf */
	#include <string.h> /* strcpy */
	#include "projet.tab.h" /* types des lexèmes, symrec, getsym et putsym. */
	void yyerror(char const *);
%}

 /**
  * Déclaration des opérateurs et liaisons (opérations en langage naturel).
  */
OPERATEURS additionner|soustraire|multiplier|diviser
LIAISONS et|par

 /**
  * Déclaration des nombres réels.
  * Ne reconnait pas des lexèmes tels que .23, mais uniquement 23, -2.3, +0.
  */
NOMBRES [+-]?(0|[1-9][0-9]*)\.?[0-9]*

 /**
  * Déclaration des identifiants.
  * Il s'agit des lexèmes commençant par une lettre ou un tiret bas, suivie d'un ou plusieurs
  * caractères alphanumériques ou tiret bas.
  */
IDENTIFIANTS _?[a-zA-Z][a-zA-Z0-9_]*

%%

{OPERATEURS} {
    strcpy(yylval.OPERATEUR, yytext); /* On récupère la valeur dans yylval en tant que OPERATEUR */
    return OPERATEUR;
}

{LIAISONS} { return LIAISON; }

{NOMBRES} {
	sscanf(yytext, "%lf", &yylval.NOMBRE); /* On récupère la valeur dans yylval en tant que NOMBRE */
 	return NOMBRE;
}

{IDENTIFIANTS} {
	/* On cherche le symbole dans la table de symboles */
	symrec *s = getsym(yytext);
	if (s == 0) { /* Si le symbole n'existe pas, on l'insère en tant que variable */
		s = putsym(yytext, VARIABLE);
	}
	*((symrec **) &yylval) = s; /* On affecte à yylval le symbole trouvé */
	return s->type; /* On renvoie le type VARIABLE ou FONCTION */
}

 /* Opérateurs mathématiques */
[-+()=/*\n] { return *yytext; } /* On renvoie juste le lexème, pas de type */

 /* Ignorer espaces blancs */
[ \t] ;

 /* Le reste est considéré comme erreur */
. yyerror("invalid character");

%%

int yywrap(void) {
 return 1;
}

